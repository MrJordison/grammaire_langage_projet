#include "include/Importer.hpp"
#include "include/AFD.hpp"
#include "include/AFN.hpp"

#include <iostream>

using namespace std;

int main()
{
    Importer * i = new Importer();
    //1e algorithme : transformation d'un AFN en AFD
    cout<<"1e algorithme : transformation d'un AFN en AFD"<<endl;
    AFN * afn = new AFN();
    afn->import("../Projet_Grammaire_et_Langages/transformtoafd.csv",i);
    AFD* transafd = afn->transformToAFD();
    cout<<"\n\nAFD résultant : \n"<<endl;
    transafd->affiche();

    //2e algorithme : minimisation d'un AFD
    cout<<"\n\n2e algorithme : minimisation d'un AFD"<<endl;
    
    AFD * a = new AFD();
    a->import("../Projet_Grammaire_et_Langages/minimisation.csv",i);
    AFD* b = a->minimisation();

    return 0;
}

