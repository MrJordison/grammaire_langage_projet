#pragma once

#include "Importer.hpp"
#include "AFD.hpp"

#include <vector>
#include <string>

class AFN{
    private:
        int initial;
        std::vector<int> finaux;
        std::vector<char> symboles;
        std::vector<std::vector<std::vector<bool> >* > transitions;

        bool isFinal(int etat);

    public:
        AFN();
        void import(std::string path, Importer * importer);
        AFD* transformToAFD();

        friend std::ostream& operator<<(std::ostream& os, const AFN& afn);
};
