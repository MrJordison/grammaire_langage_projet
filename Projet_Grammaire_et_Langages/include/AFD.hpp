#pragma once

#include "Importer.hpp"

#include <iostream>
#include <vector>
#include <string>


class AFD{
	private:
		int initial;
		std::vector<int> finaux;
		std::vector<std::vector <int> * > transitions;
		std::vector<char> symboles;
                int containsSymbol(char s);
                bool isFinal(int etat);
	public:
                AFD();
		AFD(int i, std::vector<int> & f, std::vector < std::vector<int>* > & t, std::vector<char> & s);
		bool isValid(std::string mot);

                void import(std::string path, Importer * importer);

                AFD* minimisation();
                
                //à refaire avec operator<<
                void affiche() const;

};

