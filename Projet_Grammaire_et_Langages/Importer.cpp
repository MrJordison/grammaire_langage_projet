#include <iostream>
#include <sstream>

#include "include/Importer.hpp"

using namespace std;

Importer::Importer(){
    path="";
    file = nullptr;
}

Importer::Importer(string p){
    set_path(p);
}

Importer::~Importer(){
    if(file)
        delete file;
    file = nullptr;
}

string Importer::get_path() const{
    return path;
}

void Importer::set_path(string p){
    path = p;
    file = new ifstream(path, ios::in);
    if(file) 
        cout<< "ouverture fichier : \""<< path<< "\"\n"<<endl;
    else
        cout<<" problème ouverture"<<endl;
}

ifstream& Importer::get_file() const{
    return *file;
}


//Fonction permettant de split un string d'après un char
vector<string> Importer::split(string str, char delimiter) {
    vector<string> internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;
    while(getline(ss, tok, delimiter)) {
        internal.push_back(tok);
    }               
    return internal;
}
