#include "include/AFN.hpp"

#include <cmath>
#include <map>
#include <iostream>

using namespace std;

AFN::AFN(){}

void AFN::import(string path, Importer * importer){
    importer->set_path(path);

    //variables tampons pour créer un automate
    int mode_lecture;

    string contenu("");
    int cpt = 0;
    vector<string> line_split;
    bool end = false;
    
    //Test si le fichier existe ou non
    if(importer->get_file()){
	//Parcours de toutes les lignes du fichier contenant les automates
        while(getline(importer->get_file(),contenu) && !end){
            ++cpt;
            //cout << contenu <<endl;
            switch(cpt){ 
                
                //1e ligne : dimensions du tableau des transitions 
                case 1:
                    line_split = Importer::split(contenu,'#');
                    //Remplie avec la valeur par défaut les lignes du tableau
                    for(int i(0);i<stoi(line_split[0]) ;++i)
                        transitions.push_back(
                                new vector<vector<bool> >(stoi(line_split[1]),vector<bool>(stoi(line_split[0]),false)));
                    break;

                //2e ligne : liste des états finaux de l'automate
                case 2:
                    line_split = Importer::split(contenu,'#');
                    //split de la ligne puis ajout de chaque token cast en integer dans le vecteur
                    for(string s : line_split)
                        finaux.push_back(stoi(s));
                    break;
                
                //4e ligne : liste des symboles de l'automate
                case 3:
                    line_split = Importer::split(contenu,'#');
                    
		    for(string s : line_split)
                        //ajout du symbole courant dans le tableau
                        symboles.push_back(s.at(0));
                    break;

                //5e ligne : indique le mode de lecture à appliquer pour la suite de l'automate...
                case 4:
                    mode_lecture = stoi(contenu);
                    break;
                
                default:
                    //Au delà de la 5e ligne : contient les valeurs du tableau de transitions
                    if(cpt>4 && contenu!=""){
                        line_split = Importer::split(contenu,'#');

                        //Si le mode de lecture est à 1, les valeurs sont stockées sous la forme [n° ligne, n° symbole, état suivant]
                        if(mode_lecture==1){
                            for(int j(0);j<transitions.size();++j){
                                if(line_split[2][j]==1)
                                    transitions.at(stoi(line_split[0]))->at(stoi(line_split[1]))[j]=true;
                                else
                                    transitions.at(stoi(line_split[0]))->at(stoi(line_split[1]))[j]=false;
                            }
                        }

                        //Sinon, chaque ligne suivante est une ligne complète du tableau de transition à ajouter dans l'ordre de lecture
                        else{
                            //La ligne où l'on doit ajouter les valeurs lues correspondent à la valeur du compteur - 5
                            for(int i(0);i<line_split.size();++i){
                                for(int j(0);j<transitions.size();++j){
                                    if(line_split[i][j]=='1')
                                        transitions.at(cpt-5)->at(i)[j]=true;
                                    else
                                        transitions.at(cpt-5)->at(i)[j]=false;
                                }
                            }
                        }
                    }
                    else{
                        //signifie que la ligne est vide, et donc la fin de la définition de l'automate
                        //et donc sa création et l'ajout dans le tableau retourné par la fonction
                        //réinitialisation du compteur et des variables tampons;
                        initial = 0;
                        end = true;
                    }
                    break;
            }
        }
    }
}

AFD* AFN::transformToAFD(){
    cout<<"Etat initial : "<<initial<<endl;
    cout<<"\nSymboles : ";
    for(int i(0);i<symboles.size();++i)
        cout<<symboles[i] << " ";
    cout<<endl;

    cout<<"\nEtats finaux : ";
    for(int i(0);i<finaux.size();++i)
        cout<< finaux[i];
    cout<<endl;

    cout<<"\nTransitions"<<endl;
    for(int i(0);i<transitions.size();++i){
        cout<<i << " ";
        for(int j(0);j<transitions[i]->size();++j){
            cout<< " | ";
            for(int k(0);k<transitions.size();++k)
                cout<<transitions[i]->at(j).at(k);
        }
        cout<<endl;
    }
    cout<<"\n"<<endl;

    int taille = transitions.size();
    
    vector<bool>* toEtat;
    bool etatFinal;
    bool exists;
    
    vector<pair<vector<bool>* , int> > newEtats;

    vector<int> newFinaux;

    //initialisation du nouveau tableau de transitions
    vector< vector<int>* > newTransitions;
    /*for(int i=0;i<transitions.size();++i){
        newTransitions.push_back(new vector<int>(symboles.size(),-1));
    }*/

    //Ajoute l'état initial de l'AFN dans les états de l'AFD
    vector<bool>* vec_initial = new vector<bool>(taille,false);
    vec_initial->at(0)=true;
    newEtats.push_back(pair<vector<bool>*,int>(vec_initial,0));
    newTransitions.push_back(new vector<int>(symboles.size(),-1));
    if(isFinal(0))
        newFinaux.push_back(0);

    
    //Parcours de tous les nouveaux états / supers-états de l'AFD
    for(int p(0);p<pow(2,taille);++p){
        if(p==newEtats.size())
            break;
        //pour chaque symbole de transition..
        cout<<"Nouvel état : ";
        for(int i(0);i<taille;++i)
            cout<<newEtats[p].first->at(i);
        cout<< "  | "<< newEtats[p].second<<endl;
        for(int i(0);i<symboles.size();++i){
            
            //initialisation d'un vecteur de boolean, indiquant vers quel "super-état" pointe la transition 
            //du symbole
            toEtat = new vector<bool>(taille,false);

            etatFinal = false;

            //On regarde pour chaque boolean j du tableau si celui est à 1
            //(indique quel(s) état(s) j compose(ent) le "super-état" lu en cours)
            for(int j(0);j<taille;++j){

                //si c'est le cas..
                if(newEtats[p].first->at(j)){
                    
                    //On ajoute les états pointés par les transitions au "super-état" toEtat
                    for(int k(0);k<taille;++k){
                        toEtat->at(k) = toEtat->at(k) | transitions[j]->at(i).at(k);
                        etatFinal =  etatFinal || isFinal(j);
                    }
                }
            }

            for(int j(0);j<taille;++j){
                if(toEtat->at(j))
                    etatFinal = etatFinal | isFinal(j);
            }
            //On regarde si le nouveau "super-état" obtenu est déjà dans la liste des états ou non
            exists = false;
            for(int j(0);j<newEtats.size();++j){
                if(equal(toEtat->begin(),toEtat->end(),newEtats[j].first->begin())){
                    //Mettre à l'état p  pour la transition i l'état j dans la table des transitions 
                    newTransitions[newEtats[p].second]->at(i) = newEtats[j].second;
                    exists = true;
                }
            }
            //si ce n'est pas le cas
            if(!exists){
                newEtats.push_back(pair<vector<bool>*, int>(toEtat,newEtats.size()));
                newTransitions.push_back(new vector<int>(symboles.size(),-1));
                newTransitions[newEtats[p].second]->at(i) = newEtats.size()-1;
                if(etatFinal)
                    newFinaux.push_back(newEtats.size()-1);
            }
        }
    }
    //retourne le nouvel AFD créé
    return new AFD(0,newFinaux,newTransitions,symboles);
}

bool AFN::isFinal(int etat){
    for(int f : finaux)
        if(f==etat) return true;
    return false;
}

ostream& operator<<(ostream& os, const AFN& afn){

}
