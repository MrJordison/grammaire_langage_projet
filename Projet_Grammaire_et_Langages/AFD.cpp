#include "include/AFD.hpp"

#include <algorithm>

using namespace std;

AFD::AFD(){}

AFD::AFD(int i, vector<int> & f, vector < vector<int> * > & t, vector<char> & s):initial(i){
   finaux = f;
   transitions =t;
   symboles=s;
}
bool AFD::isValid(string mot){
	//départ à l'état initial
	int etat = initial;
	//regarde à chaque lettre du mot à tester..
	for(int i(0);i<mot.length();++i){
            //...si elle sont dans la table des symboles de l'automate
            int valid = containsSymbol(mot[i]);
	    if(valid!=-1 && transitions.at(etat)->at(valid)!=-1)
                etat = transitions.at(etat)->at(valid);
	    else
                return false;

	}
	//si toutes les lettres font parti de l'alphabet, regarde si l'état récupéré est un état final
	for(int i(0);i<finaux.size();++i){
		if(etat== finaux.at(i))
		    return true;
	}
	return false;
}

int AFD::containsSymbol(char s){
    for(int i(0);i<symboles.size();++i){
        if(symboles.at(i)==s){
            return i;
        }
    }
    return -1;
}

void AFD::import(string path, Importer * importer){
    importer->set_path(path);

    //variables tampons pour créer un automate
    int mode_lecture;

    string contenu("");
    int cpt=0;
    vector<string> line_split;
    bool end = false;
    
    //Test si le fichier existe ou non
    if(importer->get_file()){
        //Parcours de toutes les lignes du fichiers contenant les automates
        while(getline(importer->get_file(),contenu) && !end){
            ++cpt;
            switch(cpt){ 
                
                //2e ligne : dimensions du tableau des transitions 
                case 1:
                    line_split = Importer::split(contenu,'#');
                    //Remplie avec la valeur par défaut les lignes du tableau
                    for(int i(0);i<stoi(line_split[0]) ;++i)
                        transitions.push_back(new vector<int>(stoi(line_split[1]),-1));
                    break;

                //3e ligne : liste des états finaux de l'automate
                case 2:
                    line_split = Importer::split(contenu,'#');
                    //split de la ligne puis ajout de chaque token cast en integer dans le vecteur
                    for(string s : line_split)
                        finaux.push_back(stoi(s));
                    break;
                
                //4e ligne : liste des symboles de l'automate
                case 3:
                    line_split = Importer::split(contenu,'#');
                    
                    for(string s : line_split){
                        //ajout du symbole courant dans le tableau
                        symboles.push_back(s.at(0));
                    }
                    break;

                //5e ligne : indique le mode de lecture à appliquer pour la suite de l'automate...
                case 4:
                    mode_lecture = stoi(contenu);
                    break;
                
                default:
                    //Au delà de la 5e ligne : contient les valeurs du tableau de transitions
                    if(cpt>4 && contenu!=""){
                        line_split = Importer::split(contenu,'#');

                        //Si le mode de lecture est à 1, les valeurs sont stockées sous la forme [n° ligne, n° symbole, état suivant]
                        if(mode_lecture==1)
                            transitions.at(stoi(line_split[0]))->at(stoi(line_split[1])) = stoi(line_split[2]);   

                        //Sinon, chaque ligne suivante est une ligne complète du tableau de transition à ajouter dans l'ordre de lecture
                        else{
                            //La ligne où l'on doit ajouter les valeurs lues correspondent à la valeur du compteur - 6
                            for(int i(0);i<line_split.size();++i)
                                transitions.at(cpt-5)->at(i)=stoi(line_split[i]);
                        }
                    }
                    else{
                        //signifie que la ligne est vide, et donc la fin de la définition de l'automate
                        //et donc sa création et l'ajout dans le tableau retourné par la fonction
                        //réinitialisation du compteur et des variables tampons;
                        initial = 0;
                        end = true;
                    }
                    break;
            }
        }
    }
}

bool AFD::isFinal(int etat){
    for(int f : finaux)
        if(etat==f) return true;
    return false;
}

AFD* AFD::minimisation(){

    vector<pair<int, int> > pile = vector<pair<int, int> >(transitions.size()^2);
    vector<int>bufferG;
    vector<int>bufferD;
    vector<vector<int> > tab_minimise;

    //initialisation du tableau de minimisation
    //1 correspond à deux états i et j différenciable, 0 non vérifiés, et -1 une case du tableau à ne pas utiliser
    for(int i(0);i<transitions.size();++i){
        tab_minimise.push_back(vector<int>(transitions.size(),0));
        for(int j(i);j<transitions.size();++j){
            tab_minimise[i][j]=-1;
        }
    }
    

   //ajout des premiers états distinguables dans la pile, cad les couples <etat final, etat non final>
    for(int f : finaux){
        for(int i(0);i<transitions.size();++i){
            if(i!=f && !isFinal(i)){
                pile.push_back(pair<int,int>(f,i));
                if(tab_minimise[i][f]!=-1)
                    tab_minimise[i][f]=1;
                else
                    tab_minimise[f][i]=1;
            }
        }
    }

    //parcours de la pile -> iteration de l'algo de minimisation
    for(int p(0);p<transitions.size()*transitions.size();++p){

        //pour chaque symbole de transition
        for(int i(0);i<symboles.size();++i){
            
            //On récupère les états dont la transition amène au couple d'états
            bufferG.clear();
            bufferD.clear();
            for(int j(0);j<transitions.size();++j){
                if(transitions[j]->at(i)==pile[p].first)
                    bufferG.push_back(j);
                if(transitions[j]->at(i)==pile[p].second)
                    bufferD.push_back(j);
            }

            //Dans le cas où les couples formés par les états trouvés ne sont pas dans la pile..
            
            if(bufferG.size()!=0 && bufferD.size()!=0){
                for(int e1 : bufferG){
                    for(int e2 : bufferD){
                        if(
                            find(pile.begin(),pile.end(), pair<int,int>(e1,e2))== pile.end() && find(pile.begin(), pile.end(),pair<int,int>(e2,e1))==pile.end()
                            ){
                            //on ajoute le couple d'états dans la pile et met à jour le tableau de minimisation
                            pile.push_back(pair<int,int>(e1,e2));

                            if(tab_minimise[e1][e2]!=-1)
                                tab_minimise[e1][e2]=1;
                            else
                                tab_minimise[e2][e1]=1;

                        }
                    }
                }
            }
        }
    }
    /*$$ affichage de la pile des couples $$*/
    cout << "Pile des couples différenciables"<<endl;
    for(pair<int, int> couple : pile)
        cout<< couple.first << " ; " << couple.second <<endl;
    cout <<"\n"<<endl;

    /*$$ affichage du tableau de transitions $$*/
    cout<< "Table de minimisation\n   |  0 |  1 |  2 |  3 |  4 |  5 |  6  "<< endl << "   ----------------------------------"<<endl;
    for(int i(0);i<transitions.size();++i){
        cout << i << "  "; 
        for(int j(0);j<transitions.size();++j){
	    if(tab_minimise[i][j] < 0)
		cout <<"| "<< tab_minimise[i][j] << " ";
	    else
		cout <<"|  "<< tab_minimise[i][j] << " ";
        }
        cout<<"\n";
    }
    cout<<"\n"<<endl;



    //Quand le tableau est complété et que tous les couples possibles ont été ajoutés et testés dans la pile,
    //on regarde dans le tableau les 0 restants, indiquant les états non différenciables
    vector<vector<int>* > newTransitions;
    vector<pair<int,int> > newEtats;
    int newInitial;

    //initialisation compteur nouveaux états
    int k=0;
    for(int i(0);i<tab_minimise.size();++i){
        for(int j(0);j<tab_minimise.size();++j){
            if(tab_minimise[i][j] ==0){
                newEtats.push_back(pair<int,int>(i,k));
                newEtats.push_back(pair<int,int>(j,k));
                //si l'un des deux états est l'état initial, le nouvel état créé sera l'état initial de l'afd minimisé
                if(i==initial || j== initial)
                    newInitial = k;
                k++;
                newTransitions.push_back(new vector<int>(symboles.size(),-1));
            }
        }
    }

    //Ajout des état restants considérés comme différenciables
    bool ajoute;
    for(int i(0);i<transitions.size();++i){
        ajoute=false;
        for(pair<int,int> n_etat : newEtats)
            ajoute= ajoute | (i==n_etat.first);
        if(!ajoute){
            newEtats.push_back(pair<int,int>(i,k));
            k++;
            newTransitions.push_back(new vector<int>(symboles.size(),-1));
            if(i== initial)
                newInitial = i;
        }
    }
    
    
    int avant =0;
    int apres=0;
    //mise à jour de la nouvelle table de transitions
    for(int i(0);i<transitions.size();++i){
        for(int j(0);j<symboles.size();++j){
            if(transitions[i]->at(j)!=-1){
                for(int k(0);k<newEtats.size();++k){
                    if(newEtats[k].first==i){
                        avant=newEtats[k].second;
                    }
                    if(newEtats[k].first==transitions[i]->at(j)){
                        apres=newEtats[k].second;
                    }
                }
                newTransitions[avant]->at(j)=apres;
            }
        }
    }
    vector<int> newFinaux;
    //récupération des états finaux
    for(int i(0);i<finaux.size();++i){
        for(pair<int,int> couple: newEtats)
            if(finaux[i]==couple.first)
                newFinaux.push_back(couple.second);
    }

    /*$$ affichage nouveaux états $$*/
    cout<< "Nouvel état initial : "<<newInitial<<endl;
    cout<< "\nNouveaux états : "<<endl;
    for(int i(0);i<newEtats.size();++i){
        cout<<" ancien : "<<newEtats[i].first<<" nouveau : "<< newEtats[i].second<< endl;
    }
    /*$$ affichage nouveaux états finaux $$*/
    cout<<"\nNouveaux états finaux : "<<endl;
    for(int i(0);i<newFinaux.size();++i)
        cout<<newFinaux[i]<<" ";
    cout<<endl;
    /*$$ affichage nouvelles transitions $$*/
    cout<<"\nNouvelles transitions : "<<endl;
    for(int i(0);i<newTransitions.size();++i){
        cout<< i << "  |";
        for(int j(0);j<transitions[i]->size();++j)
	    if(newTransitions[i]->at(j) < 0)
		cout<< newTransitions[i]->at(j)<< " |";
	    else
		cout<<" "<< newTransitions[i]->at(j)<< " |";
        cout<<endl;

    }
    return new AFD(newInitial,newFinaux,newTransitions,symboles);

}

void AFD::affiche() const{
    cout<<"Etat initial : "<<initial<<endl;
    cout<<"Etats finaux : ";
    for(int f : finaux)
        cout<<f<<" ; ";
    cout<<"\nSymboles : ";
    for(char s : symboles)
        cout<<s<<" ; ";
    cout<<"\nTransitions : "<<endl;
    for(int i(0);i<transitions.size();++i){
        cout<<i<<" | ";
        for(int j : *transitions.at(i))
            cout<<j<<" | ";
        cout<<endl;
    }
    cout<<endl;
}
